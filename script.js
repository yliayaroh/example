const users = [
    {
        id: 1,
        name: 'Yulia',
        phone: '380674567898'
    },
    {
        id: 2,
        name: 'Yulia',
        phone: '380674567898'
    },
    {
        id: 3,
        name: 'Roma',
        phone: '3806721245847'
    },
    {
        id: 4,
        name: 'Dima',
        phone: '380678889754'
    },
    {
        id: 5,
        name: 'Marta',
        phone: '380673015429'
    },
    {
        id: 6,
        name: 'Ira',
        phone: '380961899105'
    },
    {
        id: 7,
        name: 'Vasul',
        phone: '380971847415'
    },
    {
        id: 8,
        name: 'Petro',
        phone: '380966523598'
    }
]

tableCreate(users);

function tableCreate(users) {
    var tbl = document.getElementById('table');
    var tbdy = document.createElement('tbody');
    tbdy.id = 'tbody';
    for (var i = 0; i < users.length; i++) {
        var tr = document.createElement('tr');
        tr.id = 'tr';

        var th = document.createElement('th');
        th.textContent = users[i].id
        tr.appendChild(th)
        var td = document.createElement('td');
        td.textContent = users[i].name
        tr.appendChild(td)
        var td2 = document.createElement('td');
        td2.textContent = users[i].phone
        tr.appendChild(td2)
        var td3 = document.createElement('td');
        // creating button element  
        var button = document.createElement('BUTTON');

        // creating text to be 
        //displayed on button 
        var text = document.createTextNode("Edit");

        // appending text to button 
        button.appendChild(text);

        // appending button to div 
        td3.appendChild(button);;
        tr.appendChild(td3)

        tbdy.appendChild(tr);
    }
    tbl.appendChild(tbdy);
}

function sort() {
    users.reverse();
    var node = document.getElementById("tbody");
    node.parentNode.removeChild(node);
}

function searchUser(name) {

    var numbers = /^[0-9]+$/;
    let searchUsers = !name.match(numbers) ? users.filter(user => user.name.includes(name)) : users.filter(user => user.phone.includes(name));
    var node = document.getElementById("tbody");
    node.parentNode.removeChild(node);
    if (searchUsers.length < 1) {
        tableCreate(users);
    } else {
        tableCreate(searchUsers);
    }

}

function searchById() {

}

function addUser(name, phone) {
    let id = users.pop().id;
    let newUser = {
        id: ++id,
        name: name,
        phone: phone
    };
    debugger;
    users.push(newUser);
    tableCreate([newUser]);
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}