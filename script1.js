const books = [
    {
        id: 1,
        name: 'Лев Тол­стой',
        book: 'Вой­на и мир',
        year: '1869',
        page: '250',
        instance: '2'


    },
    {
        id: 2,
        name: 'Джеймс Джойс',
        book: 'Улісс',
        year: '1922',
        page: '120',
        instance: '1'


    },
    {
        id: 3,
        name: 'Уільям Фолкне',
        book: 'Шум і Злість',
        year: '1929',
        page: '165',
        instance: '3'


    },
    {
        id: 4,
        name: 'Карл Маркс',
        book: 'Капітал',
        year: '1867',
        page: '97',
        instance: '5'


    },
    {
        id: 5,
        name: 'Джон Толкієн',
        book: 'Володар колець',
        year: '1954',
        page: '265',
        instance: '8'


    },
    {
        id: 6,
        name: 'Джон Керуак',
        book: 'В дорозі',
        year: '1957',
        page: '150',
        instance: '6'


    },

]

tableCreate(books);

function tableCreate(books) {
    var tbl = document.getElementById('table');
    var tbdy = document.createElement('tbody');
    tbdy.id = 'tbody';
    for (var i = 0; i < books.length; i++) {
        var tr = document.createElement('tr');
        tr.id = 'tr';

        var th = document.createElement('th');
        th.textContent = books[i].id
        tr.appendChild(th)
        var td = document.createElement('td');
        td.textContent = books[i].name
        tr.appendChild(td)
        var td2 = document.createElement('td');
        td2.textContent = books[i].book
        tr.appendChild(td2)
        var td3 = document.createElement('td');
        td3.textContent = books[i].year
        tr.appendChild(td3)
        var td4 = document.createElement('td');
        td4.textContent = books[i].page
        tr.appendChild(td4)
        var td5 = document.createElement('td');
        td5.textContent = books[i].instance
        tr.appendChild(td5)

        var td6 = document.createElement('td');
        // creating button element  
        var button = document.createElement('BUTTON');

        // creating text to be 
        //displayed on button 
        var text = document.createTextNode("Edit");

        // appending text to button 
        button.appendChild(text);

        // appending button to div 
        td6.appendChild(button);;
        tr.appendChild(td6)

        tbdy.appendChild(tr);
    }
    tbl.appendChild(tbdy);
}

function sort() {
    books.reverse();
    var node = document.getElementById("tbody");
    node.parentNode.removeChild(node);
}

function searchBooks(name) {

    var numbers = /^[0-9]+$/;
    let searchBooks = !name.match(numbers) ? books.filter(book => books.name.includes(name)) : books.filter(book => book.year.includes(year));
    var node = document.getElementById("tbody");
    node.parentNode.removeChild(node);
    if (searchBooks.length < 1) {
        tableCreate(books);
    } else {
        tableCreate(searchBooks);
    }

}

function searchById() {

}

function addBook(name, year) {
    let id = books.pop().id;
    let newBook = {
        id: ++id,
        name: name,
        year: year
    };
    debugger;
    books.push(newBook);
    tableCreate([newBook]);
}

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}